package exemples;

import ejb.dao.exploitation.DaoExploitation;
import ejb.entites.Exploitation;
import ejb.entites.Parcelle;
import java.util.Scanner;

public class Exemple1 {

    public static void main(String[] args) {
  
         Scanner clavier = new Scanner(System.in);
        
         DaoExploitation daoExploitation= new DaoExploitation();
         System.out.print("\nEntrez l'identifiant de l'exploitation: ");
         String idEsp=clavier.next();
         
         Exploitation exp= daoExploitation.getLExploitation("exp001");
         
         System.out.println("\nLes cultures  de "+ exp.getNomExploitant()+"\n");
         
         for ( Parcelle p : exp.getLesParcelles()) {
         
              System.out.printf( "%-4s %5.2f ha %-20s\n", p.getIdParc(), +p.getSurface(),p.getlEspeceCultivee().getNomEsp());
         }
         
         System.out.println();     
    }
}