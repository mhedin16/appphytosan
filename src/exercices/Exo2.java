package exercices;

import ejb.dao.parcelle.DaoParcelle;
import ejb.dao.produit.DaoProduit;
import ejb.entites.Parcelle;
import ejb.entites.Produit;
import ejb.entites.Traitement;
import ejb.entites.TraitementEnChamp;
import java.util.Scanner;

public class Exo2 {
    

    public static void main(String[] args) {

        Scanner clavier = new Scanner(System.in);
        
        float total=0;
        
        DaoProduit daoPro = new DaoProduit();
        DaoParcelle daoPar=new DaoParcelle();

        System.out.print("\nEntrez l'identifiant du Produit: ");

        String idProd = clavier.next();
        
        Produit p = daoPro.getLeProduit(idProd);

        System.out.println("\nProduit: "+ p.getIdProd()+" "+p.getNomprod());
        
        for (Traitement t : p.getLesTraitements()){ 
            total=total+t.quantiteAppliquee();
            System.out.print(
                    "Parcelles: "+t.getLaParcelle().getIdParc()+
                    " Exploitant: "+t.getLaParcelle().getlExploitation().getNomExploitant()+
                    " Culture: "+t.getLaParcelle().getlEspeceCultivee().getNomEsp()+
                    " Quantité appliquée: "+t.quantiteAppliquee()+" kg");
            System.out.println("");
           
        }
        System.out.println("Quantité totale appliquée: "+total+" kg");
       
    }
}
