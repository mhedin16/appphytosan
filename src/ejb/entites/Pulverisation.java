
package ejb.entites;
import java.io.Serializable;
import java.util.Date;

public class Pulverisation implements Serializable {
   
    private Long  idPulv;
    
    private Date  datePulv;
    
    private Float dosagePulv; // exprimé en kg par hectare

    public Pulverisation() { }

    public Pulverisation(Long idPulv, Date datePulv, Float dosagePulv) {
        
        this.idPulv = idPulv;
        this.datePulv = datePulv;
        this.dosagePulv = dosagePulv;
    }
    
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public Long getIdPulv() {
        return idPulv;
    }
    
    public void setIdPulv(Long idPulv) {
        this.idPulv = idPulv;
    }
    
    public Date getDatePulv() {
        return datePulv;
    }
    
    public void setDatePulv(Date datePulv) {
        this.datePulv = datePulv;
    }
    
    public Float getDosagePulv() {
        return dosagePulv;
    }
    
    public void setDosagePulv(Float dosagePulv) {
        this.dosagePulv = dosagePulv;
    }
    
    public String getDatePulveChaine(){
    
     return utilitaires.UtilDate.dateVersChaine(datePulv);
    }
    //</editor-fold>
}