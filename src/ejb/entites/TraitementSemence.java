package ejb.entites;
import java.util.Date;


public class TraitementSemence  extends Traitement{
   
    private Float dosageTraitementSemence; // exprimé en kg par hectare
    
    private Date  dateTraitementSemence;

    public TraitementSemence() {
    }

    // Constructeur
    public TraitementSemence( String idTrait, Produit leProduit, Parcelle laparcelle,
                              Float dosageTraitementSemence, Date dateTraitementSemence)
    {
       
        super(idTrait, leProduit, laparcelle);
        
        this.dosageTraitementSemence   = dosageTraitementSemence;
        this.dateTraitementSemence     = dateTraitementSemence;
    }
    
    // QuantiteAppliquee
    // On l'obtient en multipliant dosageTraitementSemence par la surface de la parcelle
    // concernée
    
    @Override
    public Float quantiteAppliquee() {
        
         return getLaParcelle().getSurface()*dosageTraitementSemence;
        
    }
        
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public Float getDosageTraitementSemence() {
        return dosageTraitementSemence;
    }
    
    public void setDosageTraitementSemence(Float dosageTraitementSemence) {
        this.dosageTraitementSemence = dosageTraitementSemence;
    }
    
    public Date getDateTraitementSemence() {
        return dateTraitementSemence;
    }
    
    public void setDateTraitementSemence(Date dateTraitementSemence) {
        this.dateTraitementSemence = dateTraitementSemence;
    }
    //</editor-fold>      
}
