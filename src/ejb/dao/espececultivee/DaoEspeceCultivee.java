package ejb.dao.espececultivee;

import ejb.entites.EspeceCultivee;
import java.util.LinkedList;
import java.util.List;

import stockagemem.Donnees;

public class DaoEspeceCultivee {

    private Donnees data;
    
    
    public DaoEspeceCultivee() {
    
     data=new Donnees();
    
    }
    
   
    public EspeceCultivee getLEspeceCultivee(String id) {
       
        return data.lesEspecesCultivees.get(id);
    }

    public List<EspeceCultivee> getToutesLesEspeceCultivee() {
        
        return new LinkedList<EspeceCultivee>(data.lesEspecesCultivees.values());
    }
}