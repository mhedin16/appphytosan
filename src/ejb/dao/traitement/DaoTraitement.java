
package ejb.dao.traitement;

import ejb.entites.Traitement;
import java.util.LinkedList;
import java.util.List;

import stockagemem.Donnees;

public class DaoTraitement  {

     private  Donnees data;

    public DaoTraitement() {
        
        data=new Donnees();
    }
    
    public Traitement getLeTraitement(String id) {
       
        return data.lesTraitements.get(id);
    }

    public List<Traitement> getTousLesTraitement() {
        
        return new LinkedList<Traitement>(data.lesTraitements.values());
    }  
}