package ejb.dao.produit;

import ejb.entites.Produit;
import java.util.LinkedList;
import java.util.List;

import stockagemem.Donnees;

public class DaoProduit{
    
    private Donnees data;

    public DaoProduit() {
        
        data=  new Donnees();
    }
    
    public Produit getLeProduit(String pIdProd) {
       
        return data.lesProduits.get(pIdProd);
    }

    public List<Produit> getTousLesProduits() {
        
        return new LinkedList<Produit>(data.lesProduits.values());
    }
}