
package ejb.dao.pulverisation;

import ejb.entites.Pulverisation;
import java.util.LinkedList;
import java.util.List;

import stockagemem.Donnees;

public class DaoPulverisation {

    private  Donnees data;

    public DaoPulverisation() {
        
        data=new Donnees();
    }
     
    public Pulverisation getLaPulverisation(Long id) {
       
        return data.lesPulverisations.get(id);
    }

    public List<Pulverisation> getToutesPulverisations() {
        
        return new LinkedList<Pulverisation>(data.lesPulverisations.values());
    } 
}
